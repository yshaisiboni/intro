<?php
    include "class3.php"; // שימוש בקובץ class2.php
?>
<html>
    <head>
    <title>Object Oriented PHP Class2</title>
    </head>
    <body>
        <p>
        <?php
            $text = 'Hello World';
            echo "$text And The Universe ";
            echo '<br>';
            $msg = new Message(); // יצירת אובייקט מסוג Message
            // echo $msg->text; // את האובייקט הכנסנו למשתנה והדפסנו
            $msg->show();
            // echo Message::$count; 
            $msg1 = new Message("A new text"); // שינינו את הטקסט
            $msg1->show();
            // echo Message::$count; 
            $msg2 = new Message(); // כתיבת טקסט דיפולטיבי בגלל שריק
            $msg2->show();
            echo Message::$count; // הצגת ערך
            echo '<br>';
            $msg3 = new redMessage("a red message");
            $msg3->show();
            $msg4= new coloredMessage("a colored message");
            $msg4->color = 'red'; // נמצא במערך לכן יוצג טקסט אדום
            echo '<br>';
            $msg4->show();
            $msg4->color = 'green'; // נמצא במערך לכן יוצג טקסט ירוק
            echo '<br>';
            $msg4->show();
            $msg4->color = 'yellow'; // נמצא במערך לכן יוצג טקסט צהוב
            echo '<br>';
            $msg4->show();
            $msg4->color = 'blue'; // לא נמצא במערך לכן יוצג טקסט בצבע האחרון שהיה (צהוב
            echo '<br>';
            $msg4->show();
            $msg5= new coloredMessage("a colored message");
            $msg5->color = 'black'; // לא נמצא במערך והגדרנו משתנה חדש לכן יוצג טקסט אדום
            echo '<br>';
            $msg5->show();
            echo '<br>';
            echo "using showObject function on object";
            echo '<br>';
            showObject($msg5);
            showObject($msg1);

        ?>
        </p>
    </body>
</html>