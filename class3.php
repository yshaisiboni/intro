<?php
    class Message {
       protected $text = 'A simple message';
       public static $count = 0; // תכונה ששייכת למחלקה עצמה
       public function show(){ //שיטה
           echo "<p> $this->text </p>";
       }
       function __construct($text = ""){ // מתן ערך השמה דיפולטיבי
            ++self::$count; // בכל יצירת אובייקט מסוג הודעה (מסג') הספירה תעלה ב1
            if($text != ""){
                $this->text = $text;
            }
       }
    }

    class redMessage extends Message {
        public function show(){
            echo "<p style = 'color:red'>$this->text</p>";
        }
    }

    class coloredMessage extends Message {
        protected $color = 'red';
        public function __set($property,$value){
            if($property == 'color'){ // אני מרשה רק את הצבעים אדום צהוב וירוק
                $colors = array('red','yellow','green');
                if(in_array($value,$colors)){ //בדיקה האם הערך שהוכנס הוא אחד מהערכים שנמצאים במערך (אדום, צהוב, ירוק) ואם כן נבצע את ההשמה
                    $this->color = $value; // validation
                } 
            }
        }
        public function show(){
            echo "<p style = 'color:$this->color'>$this->text</p>";
        }
    }

function showObject($object){
    $object->show();
}

#comment 1
#comment 3d
?>